## Task List App - RESTful Web APIs
The repository is the collection of the RESTful API scripts and resources which will be provided by TaskList web server for the developers who want to integrate Task List API into their application.
Task List REST API will be based on open standards, so we can use any mobile and web development language to access the API.

##### Task List App Services
Task List's REST APIs provide access to resources (data entities) via URL paths. To use a REST API, the end application will make an HTTP request and parse the response. The response format will be in JSON. Also methods to call web services will be the standard HTTP methods like GET, PUT, POST and DELETE.

## Installation
    To get clone        : git clone https://asifhussainbit@bitbucket.org/asifhussainbit/tasklisting.git
    To install module   : npm install
    Start project       : sudo sails lift -dev   
    URL                 : http://********.com/


## POSTMAN collection

 you can simply import all url calls by importing this url into postman
 
    https://www.getpostman.com/collections/84c03ed066bbc949873c


## Configration (database)
 To change DB configuration for Task List go:
    tasklisting >> config >> connections.js
    there you can update you database information.

    as 

    mysqlServer: {
        adapter: 'sails-mysql',
        host: 'localhost',
        user: 'root', //optional
        password: '', //optional
        database: 'sailsTest' //optional
      },
    "ExpectUrl": "http://*******.com/", //see note in db_config
    "baseDir": "/home/****/***********",

    "port": 3306 // 3306 for dev env


#Usage
Following is an example of login process through the login web services of Matov app.
##Login
A login request to authenticate the application and grant access to user. To get the login response, following parameters are required.
##### Body params
- email 
- password

##### header params.
- imei
- apikey
- secretId

Resource URL:  http://*******.com/

HTTP request: GET  /Login

##### Possible Responses:
{
     "token": "17418810064c52109e2af6b6326ab5ab",
     "picture": "my_photo.jpg",
     "name": "Asif Hussain",
}

## Credits

Developed by AlphaRages

## License

Copyright (c) 2017 Asif Hussain
Licensed under the MIT license.

## Dev Deployment

1. cd tasklisting
2. git fetch
3. git checkout branchname
4. forever list (which gives you the output which includes current proccess id)
5. forever restart UID (see uid above)
6. then test use postman against the url `***.***.**.**:1337` {request type} for example - `***.***.**.**:1337/Login`


Copyright (c) 2017 Tasks Group. All rights reserved.