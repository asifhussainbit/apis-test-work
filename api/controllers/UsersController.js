/**
 * UsersController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    /**
     * User login function that calls the user login method developed in the user model pass params and a
     * callback response.

     * URL: Login/?username=asif.hussain@virtual-base.com&password=123456&imei=21321315332131&api_key=G33Ga5L8d4Je25OpbMJUIF0q01Ab04zydIWi8pvS8urIVmNrFT8&secret_id=Co9n3Od4qCN337nLmocBkhTOlxhMvS8rIEVrFT89W98A9b80ajd21bkOu
     */

    "funLogin": function(req, res) {
        "use strict";

        var params = req.params.all();

        params.ip = req.ip;
        params.imei = req.headers.imei;
        params.email = req.body.email;
        params.password = req.body.password;

        Users.UserLogin(params, function(err, resUserLogin) {
            if (resUserLogin) return res.send(200, resUserLogin);
            else return res.send(400, err);
        });
    },
};

