/**
 * Users.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
var objMD5 = require('md5');
var bcrypt = require('bcryptjs');

module.exports = {

  attributes: {

    name: {
      type: 'STRING'
    },

    email: {
      type: 'STRING',
      unique : true
    },
    password: {
      type: 'STRING',
        required: true
    },
    gender: {
      type: 'STRING'
    },
    dob: {
      type: 'Date'
    },
    picture: {
      type: 'STRING'
    },
    status: {
      type: 'INTEGER'
    },
    state: {
      type: 'STRING'
    },
    city: {
      type: 'STRING'
    },
    address: {
      type: 'STRING'
    },

    postal: {
      type: 'STRING'
    },
    country: {
      type: 'STRING'
    },

    lists: {
      collection: 'tasks',
        via: 'users'
    }
  },


    /**
     * User login function that calls the user login method developed in the user model pass params and a
     * callback response.

     * URL: Login/?email=asif.hussain@virtual-base.com&password=123456&imei=21321315332131&api_key=G33Ga5L8d4Je25OpbMJUIF0q01Ab04zydIWi8pvS8urIVmNrFT8&secret_id=Co9n3Od4qCN337nLmocBkhTOlxhMvS8rIEVrFT89W98A9b80ajd21bkOu
     */
    // Lifecycle Callbacks
    beforeCreate: function (values, cb) {
        // Hash password
        bcrypt.hash(values.password, 10, function(err, hash) {
            if(err) return cb(err);
            values.password = hash;
            //calling cb() with an argument returns an error. Useful for canceling the entire operation if some criteria fails.
            cb();
        });
    },

    "UserLogin": function(params, callback) {
        "use strict";

        var strUserFullName = "";
        var strUserPicture = "";
        var strForToken = "";

        var arrTemp = {};

        this.findOne({ email: params.email }).exec(function(err, user) {
            if(err) {
                callback(err, null);
            }
            bcrypt.compare(params.password, user.password, function(err, res) {
                if(err) callback(err, null);

                strForToken = userServices.strRandom(1, 10000000, 99999999);
                
                var strToken = objMD5(strForToken);
                App_session_tokens.create({
                    "auth_token_value": strToken,
                    "app_id": 1,
                    "user_id": user.id,
                    "imei_number": params.imei,
                    "ip_address": params.ip,
                    "status": 1
                }).exec(function(err, resDataCreateToken) {
                    if (resDataCreateToken) {
                        arrTemp.userId = user.id;
                        arrTemp.token = strToken;
                        arrTemp.userFullname = user.name;
                        arrTemp.userPicture = user.picture;

                        callback(null, UtilityServices.resSuccess(arrTemp));
                    }

                });
            });
            });

    }
};

