/**
 * App Key and secret key based authentication Policy
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
module.exports = function getAuthentication(req, res, next) {

  // User is allowed, proceed to the next policy, 
  // or if this is the last policy, the controller

  if (req.headers.apikey && req.headers.secretkey) {
      Apps_credentials.findOne({
          where: {
              app_api_key : req.headers.apikey,
              app_secret_id : req.headers.secretkey
          }
      }).exec(function (err, resDataAuthentication) {
          if (resDataAuthentication) next();
          else return res.send(400, UtilityServices.resError("E001"));
      });
  } else {
      return res.send(400, UtilityServices.resError("E001"));
  }

  // User is not allowed
  // (default res.forbidden() behavior can be overridden in `config/403.js`)

};
